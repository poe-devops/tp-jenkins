pipeline {
    agent any
    stages {
        stage("Build & UT/IT") {
            parallel {
                stage("Backend") {
                    agent {
                        docker {
                            image 'maven:3'
                            args '-v $HOME/.m2:/root/.m2'
                            args '-u root'
                            reuseNode true
                        }
                    }
                    stages {
                        stage("Build") {
                            steps {
                                dir("formation-backend") {
                                    sh 'mvn clean package -DskipTests'
                                }
                            }
                            post {
                                success {
                                    archiveArtifacts "formation-backend/target/*.jar"
                                }
                            }
                        }
                        stage("Unit & Integration Tests") {
                            steps {
                                dir("formation-backend") {
                                    sh "mvn surefire:test"
                                    junit testResults: "target/surefire-reports/*.xml", allowEmptyResults: true
                                }
                            }
                        }
                    }
                }
                stage("Frontend") {
                    stages {
                        stage("Build") {
                            agent {
                                docker {
                                    image 'node:22'
                                    reuseNode true
                                }
                            }
                            steps {
                                dir("formation-frontend") {
                                    sh "npm install"
                                    sh "npm run build -- --configuration=docker"
                                    archiveArtifacts artifacts: 'dist/**/*'
                                }
                            }
                        }
                        stage("Unit & integration tests") {
                            agent {
                                docker {
                                    image 'ghcr.io/puppeteer/puppeteer:latest'
                                    args '-u root'
                                    reuseNode true
                                }
                            }
                            steps {
                                dir("formation-frontend") {
                                    sh script: "npm run test-jenkins"
                                    junit testResults: 'reports/junit/reports.xml', allowEmptyResults: true
                                }
                            }
                        }
                    }
                }
            }
        }
        stage("SonarQube Analysis") {
            agent {
                docker {
                    image 'sonarsource/sonar-scanner-cli:latest'
                    reuseNode true
                }
            }
            steps {
                withSonarQubeEnv("sonarqube") {
                    sh 'sonar-scanner'
                }
            }
        }

        stage("Build docker image") {
            steps {
                dir('formation-backend') {
                    sh 'docker build -t 13.53.85.160:9001/formation-backend .'
                }
                dir('formation-frontend') {
                    sh 'docker build -t 13.53.85.160:9001/formation-frontend .'
                }
            }
        }

        stage('Publish docker images to nexus') {
            /* environment {
                NEXUS_CREDENTIALS = credentials('nexus-aws')
            } */
            steps {
                /* sh "docker login -u ${NEXUS_CREDENTIALS_USR} -p ${NEXUS_CREDENTIALS_PSW} 13.53.85.160:9001"
                sh 'docker push 13.53.85.160:9001/formation-backend'
                sh 'docker push 13.53.85.160:9001/formation-frontend'
                sh 'docker logout' */
                nexusArtifactUploader artifacts: [
                    [
                        artifactId: 'formations',
                         classifier: '',
                         file: 'formation-backend/target/formation-0.0.1.jar',
                          type: 'jar'
                    ]
                ],
                credentialsId: 'nexus-aws',
                groupId: 'fr.formation',
                nexusUrl: '13.53.85.160:9001',
                nexusVersion: 'nexus3',
                protocol: 'http',
                repository: 'http://13.53.85.160:9001/repository/tp-jenkins',
                version: '1.0'
            }
        }
    }
}
